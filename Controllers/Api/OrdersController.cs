﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Store_Management_App.Dtos;
using Store_Management_App.Infrastructure;
using Store_Management_App.Interfaces;
using Store_Management_App.Repositories;

namespace Store_Management_App.Controllers.Api
{
    [Authorize(Roles = "Admin")]
    public class OrdersController : ApiController
    {
        private readonly IUnitOfWork _unitOfWork;

        public OrdersController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }


        [HttpGet]
        public IHttpActionResult GetAllOrders()
        {
            return Ok(_unitOfWork.OrderRepository.GetAllOrders());

        }


        [HttpPut]
        [Route("api/orders/{id}")]
        public IHttpActionResult UpdateOrder(int id, [FromBody]UpdateOrderDto dto)
        {
           
            if (!ModelState.IsValid)
            {
                return BadRequest("Some fields are required!");
            }


            Order order = _unitOfWork.OrderRepository.GetOrder(id);

            if (order == null)
            {
                return NotFound();
            }
            _unitOfWork.OrderRepository.UpdateOrder(id, dto);
            _unitOfWork.OrderRepository.Save();
            
            return Ok(order.Status);

        }

    }

   
}