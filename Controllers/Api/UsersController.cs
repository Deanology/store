﻿using System.Web.Http;
using Store_Management_App.Dtos;
using Store_Management_App.Infrastructure;
using Store_Management_App.Interfaces;
using Store_Management_App.Repositories;

namespace Store_Management_App.Controllers.Api
{
    [Authorize(Roles = "Admin")]
    public class UsersController : ApiController
    {
        private readonly IUnitOfWork _unitOfWork;


        public UsersController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

      

        [Route("api/customers")]
        [HttpGet]
        public IHttpActionResult GetAllCustomers()
        {
           return Ok(_unitOfWork.AppUserRepository.GetAllUsers("Customer"));

        }
        [Route("api/admins")]
        [HttpGet]
        public IHttpActionResult GetAllAdmins()
        {
            return Ok(_unitOfWork.AppUserRepository.GetAllUsers("Admin"));

        }


        [HttpGet]
        public IHttpActionResult GetUser(string id)
        {
            var user = _unitOfWork.AppUserRepository.GetUser(id);
            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }

        [HttpPut]
        public IHttpActionResult UpdateUser(string id, UpdateUserDto dto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Some fields are required!");
            }

            var user = _unitOfWork.AppUserRepository.GetUser(id);
            if (user == null)
            {
                return NotFound();
            }
            _unitOfWork.AppUserRepository.UpdateUser(id, dto);
           return Ok();

        }

        [HttpDelete]
        public IHttpActionResult DeleteUser(string id)
        {
            _unitOfWork.AppUserRepository.DeleteUser(id);

            return Ok();
        }
    }
}
