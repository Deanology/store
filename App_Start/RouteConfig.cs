﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Store_Management_App
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name:null,
                url: "",
                defaults:new
                {
                    controller = "Shop", action = "Index", category = (string)null,
                    vendor = (string)null,
                    page = 1
                }
            );


            routes.MapRoute(
                name: null,
                url: "Page{page}",
                defaults: new { Controller = "Shop", action = "Index", category = (string)null , vendor = (string) null},
                new { page = @"\d+" }
            );

            routes.MapRoute(
                name: null,
                url: "Vendor/{vendor}",
                defaults: new { Controller = "Shop", action = "Index", page = 1, category = (string)null }

            );
            routes.MapRoute(
                name: null,
                url: "Category/{category}",
                defaults: new { Controller = "Shop", action = "Index" , page = 1, vendor = (string)null }
                
            );

            routes.MapRoute(
                name: null,
                url: "Vendor/{vendor}/Page{page}",
                defaults: new { Controller = "Shop", action = "Index" },
                new { page = @"\d+" }
            );

            routes.MapRoute(
                name: null,
                url: "Category/{category}/Page{page}",
                defaults: new { Controller = "Shop", action = "Index" },
                new { page = @"\d+" }
            );


            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Shop", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
