﻿namespace Store_Management_App.Interfaces
{
    public interface IUnitOfWork
    {
        IOrderRepository OrderRepository { get; }
        IVendorRepository VendorRepository { get; }
        IActivityRepository ActivityRepository { get; }
        IAppUserRepository AppUserRepository { get; }
        ICartRepository CartRepository { get; }
        ICategoryRepository CategoryRepository { get; }
        IProductRepository ProductRepository { get; }
    }
}