﻿using System.Collections.Generic;
using Store_Management_App.Models;
using Store_Management_App.Models.ViewModels;

namespace Store_Management_App.Interfaces
{
    public interface IVendorRepository
    {
        IEnumerable<Vendor> GetAllVendors();
        Vendor GetVendor(int id);
        bool UpdateVendor(int id, VendorViewModel model);
        void CreateVendor(Vendor model);
        void DeleteVendor(int id);
    }
}