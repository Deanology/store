﻿using System;
using System.Web;

namespace Store_Management_App.Infrastructure
{
    public class GuestSession
    {
        public static string SessionId()
        {
            var existingCookie = HttpContext.Current.Request.Cookies["cookie"] ?? new HttpCookie("cookie");

            if (!string.IsNullOrEmpty(existingCookie.Values["UserId"]))
            {
                return existingCookie.Values["UserId"];
            }

            existingCookie.Values.Add("UserId", HttpContext.Current.Session.SessionID);
            existingCookie.Expires = DateTime.Now.AddYears(1);
            HttpContext.Current.Response.Cookies.Add(existingCookie);
            return existingCookie.Values["UserId"];
        }
    }
}