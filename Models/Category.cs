﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Store_Management_App.Models
{
    public class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime UpdatedAt { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}