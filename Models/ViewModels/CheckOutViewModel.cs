﻿using System.Collections.Generic;

namespace Store_Management_App.Models.ViewModels
{
    public class CheckOutViewModel
    {
        public OrderViewModel OrderViewModel { get; set; }
        
        public IEnumerable<CartItem> CartItems { get; set; }
    }
}