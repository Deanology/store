﻿using System.Collections.Generic;

namespace Store_Management_App.Models.ViewModels
{
    public class ProductPageViewModel
    {
        public  IEnumerable<ProductDataView> Products { get; set; }
        public Pagination Pagination { get; set; }

        public string SelectedCategory { get; set; }

        public CartFormViewModel CartFormViewModel { get; set; }
        public string SelectedVendor { get; set; }
    }
}