﻿namespace Store_Management_App.Models.DataLayers
{
    public class ApplicationUserDateView
    {
        public string Name { get; set; }
        public string Gender { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }
}