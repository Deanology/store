﻿using System;

namespace Store_Management_App.Models.DataLayers
{
    public class OrderDataView
    {
        public int Id { get; set; }
   
        public ApplicationUserDateView User { get; set; }
        public OrderStatus Status { get; set; }
        public double Total { get; set; }
        public DateTime UpdatedAt { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}