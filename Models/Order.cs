﻿using System;
using System.Collections.Generic;
using System.Linq;
using Store_Management_App.Models;

public class Order
{
    public int Id { get; set; }
    public string UserId { get; set; }
    public ApplicationUser User { get; set; }
    public OrderStatus Status { get; set; }
    public string FullName { get; set; }
    public string CompanyName { get; set; }
    public string Address1 { get; set; }
    public string Address2 { get; set; }
    public string Town { get; set; }
    public string PostalCode { get; set; }
    public string State { get; set; }
    public string Phone { get; set; }
    public string Email { get; set; }
    public ICollection<OrderItem> OrderItems { get; set; }
    public DateTime UpdatedAt { get; set; }
    public DateTime CreatedAt { get; set; }

    public double GetTotalCost()
    {
        return OrderItems.Sum(o => o.GetCost());
    }

}