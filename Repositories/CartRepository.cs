﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Store_Management_App.Interfaces;
using Store_Management_App.Models;

namespace Store_Management_App.Repositories
{
    public class CartRepository : ICartRepository
    {
        private readonly ApplicationDbContext _context;

        public CartRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        
        public void Create(string userId)
        {
            var cart = new Cart()
            {
                UserId = userId,
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now
            };


            _context.Carts.Add(cart);
        }

        public void Save()
        {
            _context.SaveChanges();
        }
        public void AddItem(Product product, int quantity, Cart cart)
        {

            CartItem item = _context.CartItems.SingleOrDefault(c => c.CartId == cart.Id && c.ProductId == product.Id);
            if (item == null)
            {
                item = new CartItem
                {
                    CartId = cart.Id,
                    ProductId = product.Id,
                    Quantity = quantity,
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now
                };

                _context.CartItems.Add(item);
            }
            else
            {
                item.Quantity += quantity;
                item.UpdatedAt = DateTime.Now;
            }
        }

      

        public double GetTotalValue(int cartId)
        {
            return GetCartItemsFor(cartId).Sum(c => c.Product.Price * c.Quantity);
        }
        public IEnumerable<CartItem> GetCartItemsFor(int cartId)
        {
           return _context.CartItems.AsQueryable().Include(p => p.Product).Where(c => c.CartId == cartId).OrderByDescending(p => p.Id).ToList();
        }
                
        public void UpdateCart(int productId, int quantity, Cart cart)
        {
            var product = _context.CartItems.SingleOrDefault(p => p.ProductId == productId && p.CartId == cart.Id);
            if (product != null)
            {
                if (product.Quantity != quantity)
                {
                    product.Quantity = quantity;
                    product.UpdatedAt = DateTime.Now;
                }

            }
        }

        public void Clear(Cart cart)
        {
            _context.CartItems.RemoveRange(_context.CartItems.Where(c => c.CartId == cart.Id).ToList());
        }

        public void RemoveItem(Product product, Cart cart)
        {
            CartItem item = _context.CartItems.FirstOrDefault(p => p.Product.Id == product.Id && p.CartId == cart.Id);
            if (item != null)
               _context.CartItems.Remove(item);
        }

        public Cart GetCart(string userId)
        {
            return _context.Carts.Include(c => c.CartItems.Select(p =>p.Product)).SingleOrDefault(c => c.UserId == userId);
        }
    }
}