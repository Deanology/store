﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using AutoMapper;
using Store_Management_App.Infrastructure;
using Store_Management_App.Interfaces;
using Store_Management_App.Models;
using Store_Management_App.Models.ViewModels;
using Store_Management_App.Services;

namespace Store_Management_App.Repositories
{
    public class ProductRepository : IProductRepository
    {
        private readonly ApplicationDbContext _context;

        public ProductRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<ProductDataView> GetAllProducts(string category = null, string vendor = null)
        {
          return  _context.Products.AsQueryable()
              .Include(p => p.Category)
              .Include(p => p.Vendor)
              .Where(p =>
                  string.IsNullOrEmpty(category)
                  || p.Category.Name.ToLower().Equals(category.ToLower())
              ).Where(v =>
                      string.IsNullOrEmpty(vendor) || v.Vendor.Name.ToLower().Equals(vendor.ToLower()))
              .Select(
                m => new ProductDataView()
                {
                    Id = m.Id,
                    Sku = m.Sku,
                    Name = m.Name,
                    Vendor = m.Vendor.Name,
                    Category = m.Category.Name,
                    Description = m.Description,
                    Price = m.Price,
                    Quantity = m.Quantity,
                    ExpireDate = m.ExpireDate,
                    CreatedAt = m.CreatedAt,
                    UpdatedAt = m.UpdatedAt
                }).ToList();
        }

        public IEnumerable<ProductDataView> GetAllProductsPerSearch(string query)
        {
            return _context.Products.AsQueryable()
                .Include(p => p.Category)
                .Include(p => p.Vendor)
                .Where(p =>
                    string.IsNullOrEmpty(query) 
                    || p.Name.ToLower().Contains(query.ToLower()) 
                    || p.Category.Name.ToLower().Contains(query.ToLower()) 
                    || p.Vendor.Name.ToLower().Contains(query.ToLower()))
                .Select(
                    m => new ProductDataView()
                    {
                        Id = m.Id,
                        Sku = m.Sku,
                        Name = m.Name,
                        Vendor = m.Vendor.Name,
                        Category = m.Category.Name,
                        Description = m.Description,
                        Price = m.Price,
                        Quantity = m.Quantity,
                        ExpireDate = m.ExpireDate,
                        CreatedAt = m.CreatedAt,
                        UpdatedAt = m.UpdatedAt
                    }).ToList();
        }


        public IEnumerable<ProductDataView> ProductsPerPage(int pageSize, string category, string vendor, string query, int page = 1 )
        {
            if (!string.IsNullOrWhiteSpace(query))
            {
                return GetAllProductsPerSearch(query)
                    .OrderByDescending(p => p.Id)
                    .Skip((page - 1) * pageSize)
                    .Take(pageSize);
            }
            
            return GetAllProducts(category, vendor)
                .OrderByDescending(p => p.Id)
                .Skip((page - 1) * pageSize)
                .Take(pageSize);
        }

        public ProductDataView GetProductDataView(int id)
        {
            return GetAllProducts().SingleOrDefault(p => p.Id == id);
        }

        public Product GetProduct(int id)
        {
            return _context.Products.AsQueryable().Include(p => p.File).SingleOrDefault(p => p.Id == id);
        }

        public string SaveProduct(ProductViewModel model, HttpPostedFileBase photo = null)
        {

            var productInDb = GetProduct(model.Id);
            if (model.File == null && model.Id !=0)
            {
                model.File = productInDb.File;
            }

            if (photo != null)
            {
                UpsertFile.Save(model, photo);
            }

            if (model.Id == 0)
            {
                Product product = Mapper.Map<ProductViewModel, Product>(model);
                _context.Products.Add(product);
                _context.SaveChanges();

                return "new";
            }

            var prod = Mapper.Map(model, productInDb);
            prod.UpdatedAt = DateTime.Now;
            _context.SaveChanges();
            return "update";

        }

        public bool DeleteProduct(int id)
        {
            var product = GetProduct(id);

            if (product != null)
            {
                var photo = _context.Files.SingleOrDefault(f => f.Id == product.FileId);
                if (photo != null) _context.Files.Remove(photo);
                _context.Products.Remove(product);
                _context.SaveChanges();
                return true;
            }

            return false;
        }
    }
}