/*DataTable Init*/

"use strict"; 



$(document).ready(function () {
    var table = $("#admins").DataTable({
        responsive: true,
        autoWidth: false,
        language: {
            search: "",
            searchPlaceholder: "Search",
            sLengthMenu: "_MENU_items"
        },
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        ajax: {
            url: "/api/admins",
            dataSrc: ''
        },
        columns: [
            {
                data: "name"

            },
            { data: "userName" },
            { data: "email" },
            { data: "gender" },
            {
                data: "createdAt",
                render: function (data, type, admin) {
                    return moment(data).format("DD/MM/YYYY");
                }
            },
            {
                data: "updatedAt",
                render: function (data, type, admin) {
                    return moment(data).format("DD/MM/YYYY");
                }
            },
            {
                data: "id",
                render: function (data, type, admin) {
                    return '<a href="/account/edituser/' + data + '" class="mr-25" data-toggle="tooltip" data-original-title="Edit"><i class="icon-pencil"></a>';
                }
            },
            {
                data: "id",
                render: function (data) {
                    return '<a  href="#"data-admin-id="' +
                        data +
                        '" class="js-delete" data-toggle="tooltip" data-original-title="Delete" > <i class="icon-trash txt-danger"></i></a>';

                }
            }

        ]
    });
    $("#admins").on("click",
        ".js-delete",
        function () {
            var button = $(this);

            bootbox.confirm("Are you Sure You want to Delete This User?",
                function (result) {
                    if (result) {
                        $.ajax({
                            url: "/api/users/" + button.attr("data-admin-id"),
                            method: "DELETE",
                            success: function () {
                                table.row(button.parents("tr").remove()).remove().draw();

                            }

                        });
                    }
                });
        });
});



$(document).ready(function () {
    var table = $("#customers").DataTable({
        responsive: true,
        autoWidth: false,
        language: {
            search: "",
            searchPlaceholder: "Search",
            sLengthMenu: "_MENU_items"
        },
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        ajax: {
            url: "/api/customers",
            dataSrc: ''
        },
        columns: [
            {
                data: "name"

            },
            { data: "userName" },
            { data: "email" },
            { data: "gender" },
            {
                data: "createdAt",
                render: function (data, type, customer) {
                    return moment(data).format("DD/MM/YYYY");
                }
            },
            {
                data: "updatedAt",
                render: function (data, type, customer) {
                    return moment(data).format("DD/MM/YYYY");
                }
            },
            {
                data: "id",
                render: function (data, type, customer) {
                    return '<a href="/account/edituser/' + data + '" class="mr-25" data-toggle="tooltip" data-original-title="Edit"><i class="icon-pencil"></a>';
                }
            },
            {
                data: "id",
                render: function (data) {
                    return '<a  href="#"data-customer-id="' +
                        data +
                        '" class="js-delete" data-toggle="tooltip" data-original-title="Delete" > <i class="icon-trash txt-danger"></i></a>';

                }
            }

        ]
    });
    $("#customers").on("click",
        ".js-delete",
        function () {
            var button = $(this);

            bootbox.confirm("Are you Sure You want to Delete This User?",
                function (result) {
                    if (result) {
                        $.ajax({
                            url: "/api/users/" + button.attr("data-customer-id"),
                            method: "DELETE",
                            success: function () {
                                table.row(button.parents("tr").remove()).remove().draw();

                            }


                        });
                    }
                });
        });
});



$(document).ready(function () {
    var table = $("#products").DataTable({
        responsive: true,
        autoWidth: false,
        language: {
            search: "",
            searchPlaceholder: "Search",
            sLengthMenu: "_MENU_items"
        },
        ajax: {
            url: "/api/products",
            dataSrc: ''
        },

        columns: [
            {
                data: "sku",
                render: function (data, type, product) {
                    return '<a href="/dashboard/editproduct/' + product.id + '" class="mr-25" data-toggle="tooltip" data-original-title="Edit">' + data + '</a>';
                }

            },
            { data: "name" },
            { data: "category" },
            {
                data: "price",
                render: function (data, type, product) {
                    return data.toLocaleString("en-NG",
                        { style: 'currency', currency: 'NGN' });

                }

            },
            { data: "quantity" },
            {
                data: "expireDate",
                render: function (data, type, product) {
                    if (data !== null) {
                        return moment(data).format("DD/MM/YYYY");
                    } else {
                        return "None";
                    }

                }
            },
            {
                data: "createdAt",
                render: function (data, type, product) {
                    return moment(data).format("DD/MM/YYYY");
                }
            },
            {
                data: "updatedAt",
                render: function (data, type, product) {
                    return moment(data).format("DD/MM/YYYY");
                }
            },

            {
                data: "id",
                render: function (data) {
                    return '<a  href="#"data-product-id="' +
                        data +
                        '" class="js-delete" data-toggle="tooltip" data-original-title="Delete" > <i class="icon-trash txt-danger"></i></a>';

                }
            }

        ]
    });

    $("#products").on("click",
        ".js-delete",
        function () {
            var button = $(this);

            bootbox.confirm("Are you Sure You want to Delete This User?",
                function (result) {
                    if (result) {
                        $.ajax({
                            url: "/api/products/" + button.attr("data-product-id"),
                            method: "DELETE",
                            success: function () {
                                table.row(button.parents("tr").remove()).remove().draw();

                            }

                        });
                    }
                });
        });
});


$(document).ready(function () {
    var table = $("#orders").DataTable({
        responsive: true,
        autoWidth: false,
        language: {
            search: "",
            searchPlaceholder: "Search",
            sLengthMenu: "_MENU_items"
        },
        ajax: {
            url: "/api/orders",
            dataSrc: ''
        },
        columns: [

            {
                data: "id",
                render: function (data, type, order) {
                    return '<a href="/dashboard/order/' + data + '" class="mr-25" data-toggle="tooltip" data-original-title="Edit">#' + data + '</a>';
                }
            },
            { data: "user.name" },
            { data: "user.email" },
            { data: "user.gender" },
            {
                data: "total",
                render: function (data, type, order) {

                    return data.toLocaleString("en-NG",
                        { style: 'currency', currency: 'NGN' });

                }
            },
            {
                data: "status",
                render: function (data, type, order) {
                    switch (data) {
                        case 1:
                            data = '<span class="badge badge-warning">Pending</span>';
                            break;
                        case 2:
                            data = '<span class="badge badge-success">Completed</span>';
                            break;
                        case 3:
                            data = '<span class="badge badge-danger">Cancelled</span>';
                            break;

                        default:
                            break;
                    }

                    return data;
                }
            },
            {
                data: "createdAt",
                render: function (data, type, order) {
                    return moment(data).format("DD/MM/YYYY");
                }
            },
            {
                data: "updatedAt",
                render: function (data, type, order) {
                    return moment(data).format("DD/MM/YYYY");
                }
            },
            {
                data: "id",
                render: function (data, type, order) {

                    if (order.status === 1) {
                        return '<a href="#" class="mr-25 text-success js-complete" data-order-id="' + data +
                            '" data-toggle="tooltip" data-original-title="Mark as Complete" data-action="complete"> <i class="icon-check"></i> </a>'
                            + ' <a href="#" data-order-id="' + data +
                            '" class="js-cancel text-danger" data-toggle="tooltip" data-original-title="Cancel" data-action="cancel" > <i class="icon-close txt-danger"></i></a>';
                    }
                    return "";


                }
            }

        ]
    });
    $("#orders").on("click",
        ".js-complete",
        function () {
            var button = $(this);
            var model = {};
            model.Action = button.attr("data-action");

            bootbox.confirm("Are you Sure You want to mark this Order as Complete ?",
                function (result) {
                    if (result) {
                        $.ajax({
                            url: "/api/orders/" + button.attr("data-order-id"),
                            method: "PUT",
                            data: model,
                            success: function (data, res) {
                                var tbData = table.row(button.parents("tr")).data();

                                tbData.status = 2;
                                tbData.updatedAt = Date.now();
                                table.row(button.parents("tr")).invalidate().draw();
                            },
                            error: function (err) {
                                console.log(err);
                            }

                        });
                    }
                });
        });


    $("#orders").on("click",
        ".js-cancel",
        function () {
            var button = $(this);
            var model = {};
            model.Action = button.attr("data-action");

            bootbox.confirm("Are you Sure You want to cancel this Order ?",
                function (result) {
                    if (result) {
                        $.ajax({
                            url: "/api/orders/" + button.attr("data-order-id"),
                            method: "PUT",
                            data: model,
                            success: function (data, res) {
                                var tbData = table.row(button.parents("tr")).data();

                                tbData.status = 3;
                                tbData.updatedAt = Date.now();
                                table.row(button.parents("tr")).invalidate().draw();

                            },
                            error: function (err) {
                                console.log(err);
                            }


                        });
                    }
                });
        });
});